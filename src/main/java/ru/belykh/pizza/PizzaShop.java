package ru.belykh.pizza;

import org.apache.myfaces.tobago.model.SheetState;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import lombok.*;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.myfaces.tobago.component.UIRow;
import org.apache.myfaces.tobago.component.UISheet;

@ManagedBean
@SessionScoped
public class PizzaShop {


	private static final Logger LOG = LoggerFactory.getLogger(PizzaShop.class);


  @Getter @Setter
  private SheetState selectedOrders;
  
  @Getter @Setter
  private List<PizzaOrder> orders = new ArrayList<PizzaOrder>();
  
  @Getter @Setter
  private PizzaOrder selectedOrder;
  
  @Getter @Setter
  private PizzaOrder popupOrder;
  
  private ResourceBundle bundle;
  
  public PizzaShop() {
	  LOG.info("Creating Pizza shop ");
	  FacesContext context = FacesContext.getCurrentInstance();
	  bundle=context.getApplication().getResourceBundle(context, "bundle");
	  loadOrders();
  }
  
  public List<PizzaOrderStatus> getOrderStatuses(){
	  return PizzaOrderStatus.getValuesAsList();
  }
  
  public PizzaOrderStatus getDefaultOrderStatus() {
	  return PizzaOrderStatus.NEW;
  }
  
  public void selectOrder(PizzaOrder order) {
	  selectedOrder=order;
	  List<Integer> selectedRows = new ArrayList<Integer>();
	  if(order!=null) selectedRows.add(new Integer(orders.indexOf(order)));
	  selectedOrders.setSelectedRows(selectedRows);
  }
  public void selectOrder(int index) {
	  if(orders.size()>index)
		  selectOrder(orders.get(index));
	  else
		  selectOrder(null);
  }
  
  public void ordersSelectionChanged(javax.faces.event.AjaxBehaviorEvent event) throws javax.faces.event.AbortProcessingException{
	  LOG.info("Orders selection changed");
	  try {
		  UIRow row = (UIRow)event.getComponent();
		  LOG.info("Row: "+row);
		  UISheet sheet = (UISheet)row.getParent();
		  LOG.info("Sheet: "+sheet);
		  int index = sheet.getRowIndex();
		  LOG.info("Index: "+index);
		  selectOrder(index);
	  }
	  catch (Exception e) {
		  LOG.error(e.toString());
	  }
  }
  public void onAddBtnClicked(javax.faces.event.AjaxBehaviorEvent event) throws javax.faces.event.AbortProcessingException{
	  LOG.info("onAddBtnClicked(AjaxBehaviorEvent event)");
	  onAddBtnClicked();
  }
  public void onAddBtnClicked(){
	  LOG.info("onAddBtnClicked()");
	  selectOrder(null);
	  popupOrder = new PizzaOrder(0, "", "", new Date(), PizzaOrderStatus.NEW);
  }
  public void onEditBtnClicked(javax.faces.event.AjaxBehaviorEvent event) throws javax.faces.event.AbortProcessingException{
	  LOG.info("onEditBtnClicked(AjaxBehaviorEvent event)");
	  onEditBtnClicked();
  }
  public void onEditBtnClicked(){
	  LOG.info("onEditBtnClicked()");
	  popupOrder = selectedOrder;
  }
  public void onPopupSaveClicked(javax.faces.event.AjaxBehaviorEvent event) throws javax.faces.event.AbortProcessingException{
	  LOG.info("onPopupSaveClicked(AjaxBehaviorEvent event)");
	  onPopupSaveClicked();
  }
  public void onPopupSaveClicked() {
	  LOG.info("onPopupSaveClicked()");
	  boolean hasMessages=false;
	  if(popupOrder.getBuyer().length()==0) {
		  FacesContext.getCurrentInstance().addMessage("popupBuyer", new FacesMessage(FacesMessage.SEVERITY_WARN, bundle.getString("popupOrderInsertBuyer"), null));
		  hasMessages=true;
	  }
	  if(popupOrder.getAddress().length()==0) {
		  FacesContext.getCurrentInstance().addMessage("popupAddress", new FacesMessage(FacesMessage.SEVERITY_WARN, bundle.getString("popupOrderInsertAddress"), null));
		  hasMessages=true;
	  }
	  if(hasMessages) return;
	  if(selectedOrder==null) { //add
		  addOrder(popupOrder);
	  }
	  saveOrder(popupOrder);
	  popupOrder = null;
  }
  public void onPopupCancelClicked(javax.faces.event.AjaxBehaviorEvent event) throws javax.faces.event.AbortProcessingException{
	  LOG.info("onPopupCancelClicked(AjaxBehaviorEvent event)");
	  onPopupCancelClicked();
  }
  public void onPopupCancelClicked() {
	  LOG.info("onPopupCancelClicked()");
	  popupOrder = null;
  }
  
  public boolean hasSelectedOrder() {
	  return selectedOrder!=null;
  }
  
  public boolean hasPopupOrder() {
	  return popupOrder!=null;
  }
  
  private void addOrder(PizzaOrder order) {
	  orders.add(order);
  }
  
  private void loadOrders() {
	  LOG.info("loadOrders");
	  Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	  session.beginTransaction();
	  orders = (List<PizzaOrder>)  session.createSQLQuery("SELECT * FROM orders").addEntity(PizzaOrder.class).list();
	  session.getTransaction().commit();
  }
  
  
  private void saveOrder(PizzaOrder order) {
	  LOG.info("saveOrder id={}", order.getId());
	  Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	  session.beginTransaction();
	  if(order.getId()==0) {
		  session.save(order);
	  }
	  else {
		  session.update(order);
	  }
	  session.getTransaction().commit();
  }
  
}
