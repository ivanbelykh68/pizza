package ru.belykh.pizza;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum PizzaOrderStatus {
	NEW, PENDING, COMPLETE;
	private static final Logger LOG = LoggerFactory.getLogger(PizzaOrderStatus.class);
	private static final List<PizzaOrderStatus> valuesAsList;
	public String getBundleName() {
		return "pizzaOrderStatus."+this.toString();
	}
	static {
		//LOG.info("Creating valuesAsList");
		valuesAsList = new ArrayList<PizzaOrderStatus>();
		PizzaOrderStatus[] values = values();
		for(int i=0; i<values.length; i++) {
			valuesAsList.add(values[i]);
		}
	}
	public static List<PizzaOrderStatus> getValuesAsList(){
		return valuesAsList;
	}
}
