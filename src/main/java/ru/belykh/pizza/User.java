package ru.belykh.pizza;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.*;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
@Data
@AllArgsConstructor
@NoArgsConstructor

public class User {

  private static final Logger LOG = LoggerFactory.getLogger(User.class);

  private int id;

  private String login;

  private String pass;
  
}
