package ru.belykh.pizza;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.*;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
@Data
@AllArgsConstructor
@NoArgsConstructor

public class PizzaOrder {

  private static final Logger LOG = LoggerFactory.getLogger(PizzaOrder.class);


  private int id;

  private String buyer;

  private String address;

  private Date date;

  private PizzaOrderStatus status;
  
}
