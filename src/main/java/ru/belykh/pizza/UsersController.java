package ru.belykh.pizza;

import org.apache.myfaces.tobago.model.SheetState;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import lombok.*;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.myfaces.tobago.component.UIRow;
import org.apache.myfaces.tobago.component.UISheet;

@ManagedBean
@SessionScoped
public class UsersController {


	private static final Logger LOG = LoggerFactory.getLogger(UsersController.class);


  @Getter @Setter
  private SheetState selectedUsers;
  
  @Getter @Setter
  private List<User> users = new ArrayList<User>();
  
  @Getter @Setter
  private User selectedUser;
  
  @Getter @Setter
  private User popupUser;
  
  @Getter @Setter
  private String authLogin;
  
  @Getter @Setter
  private String authPass;
  
  @Getter @Setter
  private User authorisedUser;
  
  private ResourceBundle bundle;
  
  public UsersController() {
	  LOG.info("Creating UsersController ");
	  FacesContext context = FacesContext.getCurrentInstance();
	  bundle=context.getApplication().getResourceBundle(context, "bundle");
	  loadUsers();
  }
  
  
  public void selectUser(User user) {
	  selectedUser=user;
	  List<Integer> selectedRows = new ArrayList<Integer>();
	  if(user!=null) selectedRows.add(new Integer(users.indexOf(user)));
	  selectedUsers.setSelectedRows(selectedRows);
  }
  public void selectUser(int index) {
	  if(users.size()>index)
		  selectUser(users.get(index));
	  else
		  selectUser(null);
  }
  
  public void usersSelectionChanged(javax.faces.event.AjaxBehaviorEvent event) throws javax.faces.event.AbortProcessingException{
	  LOG.info("Users selection changed");
	  try {
		  UIRow row = (UIRow)event.getComponent();
		  LOG.info("Row: "+row);
		  UISheet sheet = (UISheet)row.getParent();
		  LOG.info("Sheet: "+sheet);
		  int index = sheet.getRowIndex();
		  LOG.info("Index: "+index);
		  selectUser(index);
	  }
	  catch (Exception e) {
		  LOG.error(e.toString());
	  }
  }
  public void onAddBtnClicked(javax.faces.event.AjaxBehaviorEvent event) throws javax.faces.event.AbortProcessingException{
	  LOG.info("onAddBtnClicked(AjaxBehaviorEvent event)");
	  onAddBtnClicked();
  }
  public void onAddBtnClicked(){
	  LOG.info("onAddBtnClicked()");
	  selectUser(null);
	  popupUser = new User(0, "", "");
  }
  public void onEditBtnClicked(javax.faces.event.AjaxBehaviorEvent event) throws javax.faces.event.AbortProcessingException{
	  LOG.info("onEditBtnClicked(AjaxBehaviorEvent event)");
	  onEditBtnClicked();
  }
  public void onEditBtnClicked(){
	  LOG.info("onEditBtnClicked()");
	  popupUser = selectedUser;
  }
  public void onPopupSaveClicked(javax.faces.event.AjaxBehaviorEvent event) throws javax.faces.event.AbortProcessingException{
	  LOG.info("onPopupSaveClicked(AjaxBehaviorEvent event)");
	  onPopupSaveClicked();
  }
  public void onPopupSaveClicked() {
	  LOG.info("onPopupSaveClicked()");
	  boolean hasMessages=false;
	  if(popupUser.getLogin().length()==0) {
		  FacesContext.getCurrentInstance().addMessage("popupLogin", new FacesMessage(FacesMessage.SEVERITY_WARN, bundle.getString("popupUserInsertLogin"), null));
		  hasMessages=true;
	  }
	  if(popupUser.getPass().length()==0) {
		  FacesContext.getCurrentInstance().addMessage("popupPass", new FacesMessage(FacesMessage.SEVERITY_WARN, bundle.getString("popupUserInsertPass"), null));
		  hasMessages=true;
	  }
	  if(hasMessages) return;
	  if(selectedUser==null) { //add
		  addUser(popupUser);
	  }
	  saveUser(popupUser);
	  popupUser = null;
  }
  public void onPopupCancelClicked(javax.faces.event.AjaxBehaviorEvent event) throws javax.faces.event.AbortProcessingException{
	  LOG.info("onPopupCancelClicked(AjaxBehaviorEvent event)");
	  onPopupCancelClicked();
  }
  public void onPopupCancelClicked() {
	  LOG.info("onPopupCancelClicked()");
	  popupUser = null;
  }
  
  public boolean hasSelectedUser() {
	  return selectedUser!=null;
  }
  
  public boolean hasPopupUser() {
	  return popupUser!=null;
  }
  
  private void addUser(User user) {
	  users.add(user);
  }
  
  private void loadUsers() {
	  LOG.info("loadUsers");
	  Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	  session.beginTransaction();
	  users = (List<User>)  session.createSQLQuery("SELECT * FROM users").addEntity(User.class).list();
	  session.getTransaction().commit();
  }
  
  
  private void saveUser(User user) {
	  LOG.info("saveOrder id={}", user.getId());
	  Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	  session.beginTransaction();
	  if(user.getId()==0) {
		  session.save(user);
	  }
	  else {
		  session.update(user);
	  }
	  session.getTransaction().commit();
  }
  
  public void onAuthoriseClicked(javax.faces.event.AjaxBehaviorEvent event) throws javax.faces.event.AbortProcessingException{
	  LOG.info("onAuthoriseClicked(AjaxBehaviorEvent event)");
	  onAuthoriseClicked();
  }
  public void onAuthoriseClicked() {
	  LOG.info("onAuthoriseClicked()");
	  authorise();
  }
  public boolean isAuthorised() {
	  return authorisedUser!=null;
  }
  private void authorise() {
	  LOG.info("authorise({})", authLogin);
	  
	  Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	  session.beginTransaction();
	  Query query = session.createQuery("from User where login=:login and pass=:pass");
	  query.setParameter("login", authLogin);
	  query.setParameter("pass", authPass);
	  List foundUsers = query.list();
	  if(foundUsers.size()!=0) {//at least one found
		  authorisedUser=(User)foundUsers.get(0);
		  LOG.info("authorised! {}", authLogin);
	  }
	  else {
		  FacesContext.getCurrentInstance().addMessage("authLogin", new FacesMessage(FacesMessage.SEVERITY_WARN, bundle.getString("authWrongLoginPass"), null));
		  LOG.info("unauthorise:( {}", authLogin);
	  }
	  session.getTransaction().commit();
	  authPass=null;
  }
  
}
